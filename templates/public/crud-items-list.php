<?php
	/**
	 * This template file can be overridden by
	 * {current_theme}/wcl-templates/public/crud-items-list.php
	 */
?>
<div>
	<h3 class="wcl-crud-section-title">
		<?php echo __( 'Existing Posts', 'wcl' ); ?>
		<a href="<?php echo wcl_get_page('crud')['url']; ?>?post=new" class="wcl-btn"><?php echo __( 'New', 'wcl' ); ?></a>
	</h3>

	<?php
		$existing_posts = new WP_Query( array(
			'posts_per_page' => 20,
			'status'         => 'any',
		) );

		if ( $existing_posts->have_posts() ) :
	?><ul class="wcl-posts-list">
		<?php while ( $existing_posts->have_posts() ) : $existing_posts->the_post(); ?>
		<li>
			<a href="<?php the_permalink(); ?>" class="wcl-post-title" target="_blank">
				<?php the_title(); ?>
			</a>
			<div class="wcl-actions">
				<a href="<?php echo wcl_get_page('crud')['url']; ?>?post=<?php echo get_the_ID(); ?>" class="wcl-edit">Edit</a>
				<a href="#" data-id="<?php echo get_the_ID(); ?>" class="wcl-delete wcl-delete-post">Delete</a>
			</div>
		</li>
		<?php endwhile; ?>
	</ul><?php else: ?>
	<div class="wcl-section-error">
		<?php echo __( 'No existing posts at all.', 'wcl' ); ?>
	</div>
	<?php endif;
	wp_reset_postdata();
	?>
</div>