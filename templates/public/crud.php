<?php
	/**
	 * This template file can be overridden by
	 * {current_theme}/wcl-templates/public/crud.php
	 */
	if ( ! defined( 'WEBO_CUSTOM_LOGIN_VERSION' ) ) {
		exit;
	}
	
	do_action( 'before_wcl_crud_section' );

	$wcl_crud_wrapper_classes = array( 'wcl-form-wraper', 'wcl-crud-wrapper' );
	$wcl_crud_wrapper_classes = apply_filters( 'wcl_crud_wrapper_classes', $wcl_crud_wrapper_classes );
?><div class="<?php echo implode(' ', $wcl_crud_wrapper_classes); ?>">
	<?php
		$post = isset( $_GET['post'] ) ? $_GET['post'] : false;

		if ( is_numeric( $post ) || 'new' === $post ) {
			wcl_load_template( 'public/crud-form' );
		} else {
			wcl_load_template( 'public/crud-items-list' );
		}
	?>
</div><?php do_action( 'after_wcl_crud_section' );