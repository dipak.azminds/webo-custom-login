<?php
	/**
	 * This template file can be overridden by
	 * {current_theme}/wcl-templates/public/login.php
	 */
	if ( ! defined( 'WEBO_CUSTOM_LOGIN_VERSION' ) ) {
		exit;
	}
	
	do_action( 'before_wcl_login_form' );

	$wcl_login_wrapper_classes = array( 'wcl-form-wraper', 'wcl-login-wrapper' );
	$wcl_login_wrapper_classes = apply_filters( 'wcl_login_wrapper_classes', $wcl_login_wrapper_classes );
?><div class="<?php echo implode(' ', $wcl_login_wrapper_classes); ?>">
	<form action="<?php echo WCL_ADMIN_AJAX_URL; ?>" class="wcl-form wcl-form-login" method="post">
		<div class="wcl-form-row">
			<input type="text" name="username" placeholder="<?php echo __( 'Email/username', 'wcl' ); ?>" />
		</div>
		<div class="wcl-form-row">
			<input type="password" name="password" placeholder="<?php echo __( 'Password', 'wcl' ); ?>" />
		</div>
		<div class="wcl-form-row">
			<input type="hidden" name="action" value="wcl-login" />
			<?php wp_nonce_field( 'wcl-login', '_wpnonce' ); ?>
			<?php
				$login_btn_label = apply_filters( 'wcl_login_button_label', 'Login' );
			?><button type="submit" class="wcl-submit"><?php echo __( $login_btn_label, 'wcl' ); ?></button>
		</div>
		<?php if ( $create_account_url = wcl_get_page( 'create_account' )['url'] ) : ?>
		<div class="wcl-form-row">
			<div class="wcl-from-option">
				<a href="<?php echo $create_account_url; ?>"><?php echo __( 'Create Account', 'wcl' ); ?></a>
			</div>
		</div>
		<?php endif; ?>
		<div class="form-result"></div>
	</form>
</div><?php do_action( 'after_wcl_login_form' );