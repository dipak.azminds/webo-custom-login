<?php
	/**
	 * This template file can be overridden by
	 * {current_theme}/wcl-templates/public/crud-form.php
	 */
?><div>
	<h3 class="wcl-crud-section-title">
		<?php
			$title   = ('new' === $_GET['post']) ? 'Add New Post' : 'Edit Post';
			$is_edit = ('new' !== $_GET['post']);
			echo __( $title, 'wcl' );
		?>
		<a href="<?php echo wcl_get_page('crud')['url']; ?>" class="wcl-btn"><?php echo __( 'Back', 'wcl' ); ?></a>
	</h3>

	<?php
		$title      = $content = '';
		$post_error = false;
		$api_url    = get_rest_url( null, '/wp/v2/posts' );

		if ( $is_edit ) {
			$post_id  = $_GET['post'];
			$post     = get_post( $post_id );
			$api_url .= '/' . $post_id;

			if ( $post ) {
				$title   = $post->post_title;
				$content = $post->post_content;
			} else {
				$post_error = true;
			}
		}
	?>

	<?php if ( $post_error ) : ?>
	<div class="wcl-section-error">
		<?php echo __( 'Invalid request.', 'wcl' ); ?>
	</div>
	<?php endif; ?>

	<?php if ( ! $post_error ) : ?>
	<form action="<?php echo $api_url; ?>" data-secure-api="yes" data-action="<?php echo ($is_edit) ? 'edit' : 'new'; ?>" class="wcl-form wcl-form-create-account" method="post">
		<div class="wcl-form-row">
			<div><?php echo __( 'Post Title', 'wcl' ); ?></div>
			<input type="text" autocomplete="off" value="<?php echo esc_attr( $title ); ?>" name="title" class="wcl-border" />
		</div>
		<div class="wcl-form-row">
			<div><?php echo __( 'Post Content', 'wcl' ); ?></div>
			<textarea rows="8" name="content" class="wcl-border"><?php echo esc_html( $content ); ?></textarea>
		</div>
		<div class="wcl-form-row">
			<input type="hidden" name="status" value="publish" />
			<?php
				$button_label = ($is_edit) ? 'Update' : 'Create';
				$button_label = apply_filters( 'wcl_curd_submit_button_label', $button_label );
			?><button type="submit" class="wcl-submit"><?php echo __( $button_label, 'wcl' ); ?></button>
		</div>
		<div class="form-result"></div>
	</form>
	<?php endif; ?>
</div>