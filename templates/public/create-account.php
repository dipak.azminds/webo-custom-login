<?php
	/**
	 * This template file can be overridden by
	 * {current_theme}/wcl-templates/public/create-account.php
	 */
	if ( ! defined( 'WEBO_CUSTOM_LOGIN_VERSION' ) ) {
		exit;
	}
	
	do_action( 'before_wcl_create_account_form' );

	$wcl_create_account_wrapper_classes = array( 'wcl-form-wraper', 'wcl-create-account-wrapper' );
	$wcl_create_account_wrapper_classes = apply_filters( 'wcl_create_account_wrapper_classes', $wcl_create_account_wrapper_classes );
?><div class="<?php echo implode(' ', $wcl_create_account_wrapper_classes); ?>">
	<form action="<?php echo get_rest_url( null, WCL_API_PREFIX . '/user/register' ); ?>" class="wcl-form wcl-form-create-account" method="post">
		<div class="wcl-form-row size-half">
			<input type="text" name="first_name" placeholder="<?php echo __( 'Firstname', 'wcl' ); ?>" />
		</div>
		<div class="wcl-form-row size-half">
			<input type="text" name="last_name" placeholder="<?php echo __( 'Lastname', 'wcl' ); ?>" />
		</div>
		<div class="wcl-form-row">
			<input type="text" name="user_email" placeholder="<?php echo __( 'Email', 'wcl' ); ?>" />
		</div>
		<div class="wcl-form-row">
			<input type="text" name="user_login" placeholder="<?php echo __( 'Username', 'wcl' ); ?>" />
		</div>
		<div class="wcl-form-row">
			<input type="password" name="user_pass" placeholder="<?php echo __( 'Password', 'wcl' ); ?>" />
		</div>
		<div class="wcl-form-row">
			<input type="hidden" name="_wp_nonce" value="" />
			<?php
				$create_account_btn_label = apply_filters( 'wcl_create_account_button_label', 'Create Account' );
			?><button type="submit" class="wcl-submit"><?php echo __( $create_account_btn_label, 'wcl' ); ?></button>
		</div>
		<?php if ( $login_url = wcl_get_page( 'login' )['url'] ) : ?>
		<div class="wcl-form-row">
			<div class="wcl-from-option">
				<a href="<?php echo $login_url; ?>"><?php echo __( 'Login', 'wcl' ); ?></a>
			</div>
		</div>
		<?php endif; ?>
		<div class="form-result"></div>
	</form>
</div><?php do_action( 'after_wcl_create_account_form' );