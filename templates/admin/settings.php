<?php
	/**
	 * This template file can be overridden by
	 * {current_theme}/wcl-templates/admin/settings.php
	 */
?>
<div class="wrap">
    <h1><?php echo __( 'WCL Settings', 'wcl' ); ?></h1>
    <form method="post" action="options.php">
    <?php
        settings_fields( 'wcl_options_group' );
        do_settings_sections( 'wcl-settings' );
        submit_button();
    ?>
    </form>
</div>