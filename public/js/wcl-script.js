(function($){

	function is_form_loading(form){
		return form.attr('loading') === 'yes';
	}

	function set_form_loading(form) {
		form.attr('loading', 'yes');

		var submitBtn = form.find('.wcl-submit');

		submitBtn
			.data('pre-text', submitBtn.text())
			.text('Wait..')
			.addClass('wcl-disabled');
	}

	function remove_form_loading(form) {
		form.removeAttr('loading');

		var submitBtn = form.find('.wcl-submit');

		submitBtn
			.text(submitBtn.data('pre-text'))
			.removeAttr('data-pre-text')
			.removeClass('wcl-disabled');
	}

	function set_form_message(form, message, type){
		var holder = form.find('.form-result').html(message);

		if ( type === 'error' ) {
			holder.removeClass('success').addClass('error');
		} else if ( type === 'success' ) {
			holder.removeClass('error').addClass('success');
		} else {
			holder.removeClass('error').removeClass('success');
		}
	}

	function handle_api_response(form, response){
		if ( response.code === 'rest_missing_callback_param' ) {
			set_form_message(
				form,
				'Some fields are missing, please reload the page.',
				'error'
			);
		}

		if ( response.code === 'rest_invalid_param' ) {
			for ( var i = 0; i < response.message.length; i++ ) {
				var filedName = response.message[i].name;
				var message = response.message[i].error;

				var field = form.find('input[name="'+filedName+'"]');

				field.closest('.wcl-form-row').append('<div class="wcl-field-error">' + message + '</div>');
			}
		}

		if ( response.code === 'user_create_failed' || response.code === 'error_message' ) {
			set_form_message(
				form,
				response.message,
				'error'
			);
		}

		if ( response.code === 'user_created' || response.code === 'success_message' ) {
			set_form_message(
				form,
				response.message,
				'success'
			);

			form.resetForm();
		}

		if ( response.code === 'rest_cookie_invalid_nonce' || response.code === 'rest_cannot_create' ) {
			set_form_message(
				form,
				'You may have been logged out, please reload to make sure.',
				'error'
			);
		}

		if ( response.code === 'empty_content' ) {
			set_form_message(
				form,
				'Post content is empty.',
				'error'
			);
		}

		if ( form.data('action') === 'new' && typeof response.id !== 'undefined' ) {
			var post_id  = String(response.id);
			var edit_url = wpApiSettings.crud_url + '?post=' + post_id;

			set_form_message( form, 'Post added.', 'success' );
			setTimeout( function(){ document.location.href = edit_url; }, 1000 );
			return;
		}

		if ( form.data('action') === 'edit' && typeof response.id !== 'undefined' ) {
			set_form_message( form, 'Post updated.', 'success' );
		}

		var redirect = (typeof response.redirect !== 'undefined') ? response.redirect : false;

		if ( redirect ) {
			setTimeout( function(){
				document.location.href = redirect;
			}, 1000 );
		} else {
			remove_form_loading(form);
		}
	}

	$(document).on('submit', '.wcl-form', function(event){
		var form    = $(this);
		var headers = {};

		if ( form.data('secure-api') === 'yes' ) {
			headers['X-WP-Nonce'] = wpApiSettings.nonce;
		}

		$(this).ajaxSubmit({
			headers: headers,

			beforeSend: function(){
				if ( is_form_loading( form ) ) {
					return false;
				}

				form.find('.wcl-field-error').remove();
				set_form_message(form, '');
				set_form_loading(form);
			},

			error: function(res){
				handle_api_response(form, res.responseJSON);
			},

			success: function(data){
				handle_api_response(
					form,
					typeof data === 'object' ? data : JSON.parse(data)
				);
			}
		});

		event.preventDefault();
		return false;
	});

	function handle_post_delete_api_response(btn, id, response) {
		if ( id == response.id ) {
			btn.closest('li').remove();
			return;
		}

		alert('You may have been logged out, please reload to make sure.');
	}

	$(document).on('click', '.wcl-delete-post', function(event){
		var btn        = $(this);
		var id         = btn.data('id');
		var delete_url = wpApiSettings.root + 'wp/v2/posts/' + id;

		if ( confirm( 'Sure to delete?' ) ) {
			$(this).ajaxSubmit({
				url: delete_url,
				method: 'DELETE',
				headers: {
					'X-WP-Nonce': wpApiSettings.nonce
				},
				error: function(res){
					handle_post_delete_api_response(btn, id, res.responseJSON);
				},
				success: function(data){
					handle_post_delete_api_response(
						btn,
						id,
						typeof data === 'object' ? data : JSON.parse(data)
					);
				}
			});
		}

		event.preventDefault();
		return false;
	});

})(jQuery);
