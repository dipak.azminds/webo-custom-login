<?php
/**
 * @wordpress-plugin
 * Plugin Name:       WebO Custom Login
 * Plugin URI:        https://example.com
 * Description:       Custom login and signup forms implemented with shortcodes.
 * Version:           1.0.0
 * Author:            Dipak Acharya
 * Author URI:        https://wordpress.org
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wcl
 */

namespace WebO_Custom_Login;

if ( ! defined( 'WPINC' ) ) {
	exit;
}

! defined( 'WEBO_CUSTOM_LOGIN_VERSION' ) && define( 'WEBO_CUSTOM_LOGIN_VERSION', '1.0.0' );
! defined( 'WCL_ADMIN_AJAX_URL' ) && define( 'WCL_ADMIN_AJAX_URL' , admin_url( 'admin-ajax.php' ) );
! defined( 'WCL_API_PREFIX' ) && define( 'WCL_API_PREFIX' , 'wcl/v1' );
! defined( 'WCL_NEW_USER_ROLE' ) && define( 'WCL_NEW_USER_ROLE' , 'author' );

if ( ! class_exists( 'WebO_Custom_Login' ) ) {

	class WebO_Custom_Login {

		public function __construct() {
			$this->init_class_autoloads();
			$this->load_includes();
			$this->init_admin_options();
			$this->init_shortcodes();

			add_action( 'template_redirect', array( $this, 'wcl_page_redirects' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_public_assets' ) );
			add_action( 'rest_api_init', array( $this, 'init_rest_apis' ) );
		}

		public function init_class_autoloads() {
			spl_autoload_register( function($class_name) {
				$filename   = str_replace('WebO_Custom_Login\\', '', $class_name);
				$filename   = str_replace('_', '-', $filename);
				$filename   = sprintf( 'class-%s.php', strtolower( $filename ) );
				$class_file = __DIR__ . '/classes/' . $filename;

				if ( file_exists( $class_file ) ) {
					require_once $class_file;
				}
			} );
		}

		public function load_includes() {
			require_once __DIR__ . '/inc/template-helpers.php';
		}

		public function init_admin_options() {
			new WCL_Plugin_Options();
		}

		public function init_shortcodes() {
			new WCL_Shortcode_Login();
			new WCL_Shortcode_Create_Account();
			new WCL_Shortcode_CRUD();
		}

		public function init_rest_apis() {
			new WCL_Register_User_API();
		}

		public function wcl_page_redirects() {
			$crud_page           = wcl_get_page( 'crud' );
			$login_page          = wcl_get_page( 'login' );
			$create_account_page = wcl_get_page( 'create_account' );

			if ( $crud_page['id'] && is_page( $crud_page['id'] ) && ! is_user_logged_in() ) {
				if ( $login_url = $login_page['url'] ) {
					wp_redirect( $login_url );
					exit;
				}

				echo __( 'You need to login to access this page.', 'wcl' );
			    exit;
			}

			if ( $login_page['id'] && is_page( $login_page['id'] ) && is_user_logged_in() ) {
				if ( $crud_url = $crud_page['url'] ) {
					wp_redirect( $crud_url );
					exit;
				}

				echo __( 'You need to logout to be able to login again.', 'wcl' );
				exit;
			}

			if ( $create_account_page['id'] && is_page( $create_account_page['id'] ) && is_user_logged_in() ) {
				if ( $crud_url = $crud_page['url'] ) {
					wp_redirect( $crud_url );
					exit;
				}

				echo __( 'You need to logout to be able create an account.', 'wcl' );
				exit;
			}
		}

		public function enqueue_public_assets() {
			$jquery_form_url = \plugin_dir_url(__FILE__) . 'public/js/jquery.form.min.js';
			wp_enqueue_script( 'malsup-jquery-form', $jquery_form_url, array( 'jquery' ), null, true );

			$wcl_script_url = \plugin_dir_url(__FILE__) . 'public/js/wcl-script.js';
			wp_enqueue_script( 'wcl-script', $wcl_script_url, array( 'malsup-jquery-form' ), WEBO_CUSTOM_LOGIN_VERSION, true );

			$wcl_style_url = \plugin_dir_url(__FILE__) . 'public/css/wcl-style.css';
			wp_enqueue_style( 'wcl-style', $wcl_style_url, array(), WEBO_CUSTOM_LOGIN_VERSION, false );

			wp_localize_script( 'wp-api', 'wpApiSettings', array(
				'root'     => esc_url_raw( rest_url() ),
				'nonce'    => wp_create_nonce( 'wp_rest' ),
				'crud_url' => wcl_get_page( 'crud' )['url']
			) );

			wp_enqueue_script('wp-api');
		}

	}

	new WebO_Custom_Login();

}