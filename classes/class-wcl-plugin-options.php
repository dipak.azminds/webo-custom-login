<?php

namespace WebO_Custom_Login;

if ( ! class_exists( 'WCL_Plugin_Options' ) ) {

	class WCL_Plugin_Options {
	    private $options;

	    public function __construct() {
	        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
	        add_action( 'admin_init', array( $this, 'page_init' ) );
	    }

	    public function add_plugin_page() {
	        \add_menu_page(
				'WCL Settings',
				'WCL',
				'administrator',
				'wcl-settings',
				array( $this, 'render_settings_page' ),
				'dashicons-chart-area',
				26
			);
	    }

	    public function render_settings_page() {
	        $this->options = get_option( 'wcl_options' );
	        wcl_load_template( 'admin/settings' );
	    }

	    public function page_init() {        
	        register_setting(
	            'wcl_options_group', // Option group
	            'wcl_options', // Option name
	            array( $this, 'sanitize' ) // Sanitize
	        );

	        add_settings_section(
	            'wcl_general_settings_section',
	            '', // Title
	            false,
	            'wcl-settings' // Page
	        );  

	        add_settings_field(
	            'login_page_id', // ID
	            'Login Page ID', // Title 
	            array( $this, 'field_login_page_id' ), // Callback
	            'wcl-settings', // Page
	            'wcl_general_settings_section' // Section           
	        );

	        add_settings_field(
	            'create_account_page_id', 
	            'Create Account Page ID', 
	            array( $this, 'field_create_account_page_id' ), 
	            'wcl-settings', 
	            'wcl_general_settings_section'
	        );

	        add_settings_field(
	            'crud_page_id', 
	            'CRUD Page ID', 
	            array( $this, 'field_crud_page_id' ), 
	            'wcl-settings', 
	            'wcl_general_settings_section'
	        );
	    }

	    /**
	     * Sanitize each setting field as needed
	     *
	     * @param array $input Contains all settings fields as array keys
	     */
	    public function sanitize($input) {
	        return $input;
	    }

	    public function field_login_page_id() {
	    	printf(
	            '<input type="number" id="login_page_id" name="wcl_options[login_page_id]" value="%s" />',
	            isset( $this->options['login_page_id'] ) ? esc_attr( $this->options['login_page_id']) : ''
	        );
	    }

	    public function field_create_account_page_id() {
	    	printf(
	            '<input type="number" id="create_account_page_id" name="wcl_options[create_account_page_id]" value="%s" />',
	            isset( $this->options['create_account_page_id'] ) ? esc_attr( $this->options['create_account_page_id']) : ''
	        );
	    }

	    public function field_crud_page_id() {
	    	printf(
	            '<input type="number" id="crud_page_id" name="wcl_options[crud_page_id]" value="%s" />',
	            isset( $this->options['crud_page_id'] ) ? esc_attr( $this->options['crud_page_id']) : ''
	        );
	    }
	}

}