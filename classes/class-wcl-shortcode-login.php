<?php

namespace WebO_Custom_Login;

if ( ! class_exists( 'WCL_Shortcode_Login' ) ) {

	class WCL_Shortcode_Login {

		public function __construct() {
			add_shortcode( 'wcl_login', array( $this, 'render_form' ) );
			add_action( 'wp_ajax_wcl-login', array( $this, 'handle_login' ) );
			add_action( 'wp_ajax_nopriv_wcl-login', array( $this, 'handle_login' ) );
		}

		public function render_form() {
			ob_start();
			wcl_load_template( 'public/login' );
			return ob_get_clean();
		}

		private function response($code, $data, $status = 200, $redirect = '') {
			$res = array(
				'code' => $code,
				'data' => array(
					'status' => $status,
				),
				'message' => $data
			);

			if ( $redirect ) {
				$res['redirect'] = $redirect;
			}
			
			wp_send_json($res, $status, 0);
		}

		private function verify_nonce() {
			if ( ! isset( $_POST['_wpnonce'] ) || 1 !== wp_verify_nonce( $_POST['_wpnonce'], 'wcl-login' ) ) {
				$this->response(
					'error_message',
					__( 'The request seems to be broken, please reload the page.', 'wcl' ),
					400
				);
			}
		}

		private function validate_request() {
			$username = isset( $_POST['username'] ) ? trim( $_POST['username'] ) : null;
			$password = isset( $_POST['password'] ) ? trim( $_POST['password'] ) : null;

			if ( null === $username || null === $password ) {
				$this->response(
					'rest_missing_callback_param',
					__( 'Login information is missing.', 'wcl' ),
					400
				);
			}

			$validation_errors = array();

			if ( empty( $username ) ) {
				$validation_errors[] = array(
					'name'  => 'username',
					'error' => __( 'Username/email required.', 'wcl' ),
				);
			}

			if ( empty( $password ) ) {
				$validation_errors[] = array(
					'name'  => 'password',
					'error' => __( 'Password required.', 'wcl' ),
				);
			}

			if ( 0 < count( $validation_errors ) ) {
				$this->response(
					'rest_invalid_param',
					$validation_errors,
					400
				);
			}

			return compact( 'username', 'password' );
		}

		public function handle_login() {
			$this->verify_nonce();

			$userdata     = $this->validate_request();
			$login_result = \wp_signon( array(
				'user_login'    => $userdata['username'],
				'user_password' => $userdata['password'],
			), true );

			if ( \is_wp_error( $login_result ) ) {
				$this->response(
					'error_message',
					__( 'Invalid email/username or password', 'wcl' ),
					400
				);
			}

			$redirect_url = \wcl_get_page( 'crud' )['url'];

			$this->response(
				'success_message',
				__( 'Logged in successfully' ),
				200,
				$redirect_url
			);
		}
	}

}