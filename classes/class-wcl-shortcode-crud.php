<?php

namespace WebO_Custom_Login;

if ( ! class_exists( 'WCL_Shortcode_CRUD' ) ) {

	class WCL_Shortcode_CRUD {

		public function __construct() {
			add_shortcode( 'wcl_crud', array( $this, 'render_form' ) );
		}

		public function render_form() {
			ob_start();
			wcl_load_template( 'public/crud' );
			return ob_get_clean();
		}
		
	}

}