<?php

namespace WebO_Custom_Login;

if ( ! class_exists( 'WCL_Register_User_API' ) ) {

	class WCL_Register_User_API {

		public function __construct() {
			$this->register_api();
		}

		private function register_api() {
			register_rest_route( WCL_API_PREFIX, 'user/register', array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'handle_create_account_api_request' ),
				'permission_callback' => '__return_true',
				'args'                => array(
					'first_name' => array(
		            	'required'          => true,
		            	'type'              => 'string',
		            	'sanitize_callback' => array( $this, 'trim_spaces' ),
		            ),

		            'last_name' => array(
		            	'required'          => true,
		            	'type'              => 'string',
		            	'sanitize_callback' => array( $this, 'trim_spaces' ),
		            ),

		            'user_email' => array(
		            	'required'          => true,
		            	'type'              => 'string',
		            	'sanitize_callback' => array( $this, 'trim_spaces' ),
		            	// 'format'   => 'email',
		            ),

		            'user_login' => array(
		            	'required'          => true,
		            	'type'              => 'string',
		            	'sanitize_callback' => array( $this, 'trim_spaces' ),
		            ),

		            'user_pass' => array(
		            	'required'          => true,
		            	'type'              => 'string',
		            	'sanitize_callback' => array( $this, 'trim_spaces' ),
		            ),
				)
			) );
		}

		public function trim_spaces($value) {
			return trim($value);
		}

		private function invalid_email($email_address) {
			if ( empty( $email_address ) ) {
				return __( 'Email required.', 'wcl' );
			}

			if ( ! filter_var( $email_address, FILTER_VALIDATE_EMAIL ) ) {
				return __( 'Invalid email.', 'wcl' );
			}

			if ( \email_exists( $email_address ) ) {
				return __( 'Email already exists.', 'wcl' );
			}

			return false;
		}

		private function invalid_username($username) {
			if ( empty( $username ) ) {
				return __( 'Username required.', 'wcl' );
			}

			if ( \username_exists( $username ) ) {
				return __( 'Username already exists.', 'wcl' );
			}

			return false;
		}

		private function invalid_password($password) {
			if ( empty( $password ) ) {
				return __( 'Password required.', 'wcl' );
			}

			if ( substr_count( $password, ' ' ) ) {
				return __( 'Spaces are not allowed in password.', 'wcl' );
			}

			if ( 4 > strlen($password) ) {
				return __( 'Password should be at least 4 characters long.', 'wcl' );
			}

			return false;
		}

		private function validate_fields($request){
			$first_name = $request->get_param( 'first_name' );
			$last_name  = $request->get_param( 'last_name' );
			$user_email = $request->get_param( 'user_email' );
			$user_login = $request->get_param( 'user_login' );
			$user_pass  = $request->get_param( 'user_pass' );

			$validation_errors = array();

			if ( empty( $first_name ) ) {
				$validation_errors[] = array(
					'name'  => 'first_name',
					'error' => __( 'First name required.', 'wcl' ),
				);
			}

			if ( empty( $last_name ) ) {
				$validation_errors[] = array(
					'name'  => 'last_name',
					'error' => __( 'Last name required.', 'wcl' ),
				);
			}

			if ( $error = $this->invalid_email( $user_email ) ) {
				$validation_errors[] = array(
					'name'  => 'user_email',
					'error' => $error
				);
			}

			if ( $error = $this->invalid_username( $user_login ) ) {
				$validation_errors[] = array(
					'name'  => 'user_login',
					'error' => $error
				);
			}

			if ( $error = $this->invalid_password( $user_pass ) ) {
				$validation_errors[] = array(
					'name'  => 'user_pass',
					'error' => $error
				);
			}

			return array(
				'errors'   => ( 0 === count( $validation_errors ) ) ? false : $validation_errors,
				'userdata' => compact(
					'first_name',
					'last_name',
					'user_email',
					'user_login',
					'user_pass'
				)
			);
		}

		public function handle_create_account_api_request($request) {
			$result = $this->validate_fields( $request );

			if ( $validation_errors = $result['errors'] ) {
				return new \WP_Error( 'rest_invalid_param', $validation_errors, array(
					'status' => 400
				) );
			}

			$result['userdata']['role'] = \apply_filters( 'wcl_new_user_role', WCL_NEW_USER_ROLE );

			return $this->create_account( $result['userdata'] );
		}

		private function create_account($data) {
			$result = wp_insert_user( $data );

			if ( \is_wp_error( $result ) ) {
				return new \WP_Error( 'user_create_failed', __( $result->get_error_message(), 'wcl' ), array(
					'status' => 400
				) );
			}

			return array(
				'code'    => 'user_created',
				'message' => __( 'Account successfully created.', 'wcl' ),
				'data'    => array(
					'status' => 200
				)
			);
		}
	}

}
