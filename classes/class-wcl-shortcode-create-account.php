<?php

namespace WebO_Custom_Login;

if ( ! class_exists( 'WCL_Shortcode_Create_Account' ) ) {

	class WCL_Shortcode_Create_Account {

		public function __construct() {
			add_shortcode( 'wcl_create_account', array( $this, 'render_form' ) );
		}

		public function render_form() {
			ob_start();
			wcl_load_template( 'public/create-account' );
			return ob_get_clean();
		}
	}

}