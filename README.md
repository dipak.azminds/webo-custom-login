# WordPress custom login and CRUD implemention

This is a standard WordPress plugin I created to enable guests to create an account, login and add/edit/delete WordPress posts. I took an advantage of all buit-in WordPress functions and hooks to achieve what's in the plugin.

## Inside the plugin

When you carefully look at the code, you will be able to see these utilized techniques.

1. Shortcodes
2. Buit-in REST APIs
3. API authencation using cookies and WP nonce technique
4. Custom REST APIs
5. Custom AJAX endpoints
6. jQuery and AJAX
7. Form validation

## Available shortcodes
#### For create account form
```[wcl_create_account]```
#### For login form
```[wcl_login]```
#### For CRUD page
```[wcl_crud]```

## Video Tutorial
[https://drive.google.com/file/d/1NnC3fHqDvBAxrEkU4Ni1IIVngRkcwiQQ/view?usp=sharing](https://drive.google.com/file/d/1NnC3fHqDvBAxrEkU4Ni1IIVngRkcwiQQ/view?usp=sharing)

## Disclaimer

I created this plugin essentially for a test purpose but not for a production purpose. Therefore, I didn't go through detailed QA tests etc. So this plugin might have a few issues or you might find something beyond your expectations, but, I am happy to discuss in the next meeting on things you need more clarification.
