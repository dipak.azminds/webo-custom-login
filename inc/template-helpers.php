<?php

function wcl_load_template( $template_name ) {
	// Look in the child theme directory.
	$location1 = get_stylesheet_directory_uri() . '/wcl-templates/' . $template_name . '.php';

	if ( file_exists( $location1 ) ) {
		include $location1;
		return;
	}

	// Look in the parent theme directory.
	$location2 = get_template_directory_uri() . '/wcl-templates/' . $template_name . '.php';

	if ( file_exists( $location2 ) ) {
		include $location2;
		return;
	}

	// Inside plugin directory, default location.

	$location3 = dirname( __DIR__ ) . '/templates/' . $template_name . '.php';

	if ( file_exists( $location3 ) ) {
		include $location3;
		return;
	}

	echo sprintf( '<div class="wcl-template-error">%s: %s</div>', __( 'WCL template error', 'wcl' ), $template_name );
}

function wcl_get_page($name) {
	$options     = get_option('wcl_options');
	$option_name = $name . '_page_id';
	$page_id     = isset( $options[$option_name] ) ? $options[$option_name] : false;
	$page_url    = ($page_id) ? get_the_permalink($page_id) : false;

	return array(
		'id'  => $page_id,
		'url' => $page_url,
	);
}